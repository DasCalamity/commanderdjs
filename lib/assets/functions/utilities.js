module.exports = (commander) => {

  commander.permlevel = message => {

    let permlvl = 0;

    if (message.author.id === commander.config.ownerID) return 10;
    if (!message.guild || !message.member) return 0;

    try {
      const mPerms = this.commander.permCheck(message, "KICK_MEMBERS" || "MANAGE_MESSAGES");
      if (!mPerms.length) permlvl = 1;
    } catch (e) {
      console.warn("modRole not present in guild settings. Skipping Moderator (level 2) check");
    }
   
    try {
      const Perms = this.commander.permCheck(message, "BAN_MEMBERS" || "ADMINISTRATOR");
      if (!Perms.length) permlvl = 2;
    } catch (e) {
      console.warn("adminRole not present in guild settings. Skipping Administrator (level 3) check");
    }

    if (message.author.id === message.guild.owner.id) permlvl = 3;
    if (commander.config.devs.includes(message.author.id)) permlvl = 5;
    if (message.author.id === commander.user.id) permlvl = 6;

    return permlvl;
  
  };

  commander.permCheck = (message, perms) => {
    if (message.channel.type !== 'text') return;
    return message.channel.permissionsFor(message.guild.me).missing(perms);
  }

  commander.log = (type, msg, title) => {
    if (!title) title = "Log";
    console.log(`[${type}] [${title}]${msg}`);
  };

  commander.loadCommand = (commandPath, commandName) => {
    try {
      const path = require('path');
      const props = new (require(`${commandPath}${path.sep}${commandName}`))(commander);
      
      props.conf.location = commandPath;
      
      commander.log('Commander', `Loading Command: ${props.help.name}. 👌`);
  
      if (props.init) props.init(commander);
      
      commander.commands.set(props.help.name, props);
      props.conf.aliases.forEach(alias => commander.aliases.set(alias, props.help.name));
      
      return false;
    } catch (e) {
      return commander.log(`Commander`, `Unable to load command ${commandName}: ${e}`);
    }
  }

  commander.unloadCommand = async (commandPath, commandName) => {
    let command;
    if (commander.commands.has(commandName)) {
      command = commander.commands.get(commandName);
    } else if (commander.aliases.has(commandName)) {
      command = commander.commands.get(commander.aliases.get(commandName));
    }
    if (!command) return `The command \`${commandName}\` doesn"t seem to exist, nor is it an alias. Try again!`;
    
    if (command.shutdown) {
      await command.shutdown(commander);
    }
    delete require.cache[require.resolve(`${commandPath}/${commandName}.js`)];
    return false;
  };

  commander.hastebin = async (input, extension) => {
    const snekfetch = require("snekfetch");
    return new Promise(function (res, rej) {
    if (!input) rej("Input argument is required.");
      snekfetch.post("https://hastebin.com/documents").send(input).then(body => {
        res("https://hastebin.com/" + body.body.key + ((extension) ? "." + extension : ""));
      }).catch(e => rej(e));
    })
  };

  commander.awaitReply = async (message, question, limit = 30000) => {
    const disagree = ['cancel'];
    const filter = m => m.author.id === message.author.id;

    await message.reply(question + "\n\nRespond with ``cancel`` to cancel the command. The command will automatically be cancelled in 30 seconds.");
    
    try {
      const collected = await message.channel.awaitMessages(filter, { max: 1, time: limit, errors: ['time'] });
      
      if (disagree.includes(collected.first().content.toLowerCase())) {
        message.reply('Command was Cancelled!');
        return false 
      }
      return collected.first().content;
    } catch (error) {
      message.reply('Command was Cancelled!');
      return false;
    }
  };

  global.wait = require("util").promisify(setTimeout);

  global.error = (message, error, explain, fix) => {
    const {MessageEmbed} = require('discord.js');
    const err = new MessageEmbed()
      .setThumbnail("https://i.imgur.com/oGKgyPs.png")
      .setColor(0xC4010E)
      .addField("Error", error)
      .addField("Whats Going On?", explain)
      .addField("How to Fix", fix)
    return message.channel.send({embed: err})
  }

  global.range = (count, start = 0) => {
    const myArr = [];
    for (var i = 0; i<count; i++) {
      myArr[i] = i+start;
    }
    return myArr;
  };

  process.on('uncaughtException', (err) => {
    const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, 'g'), './');
    console.error('Uncaught Exception: ', errorMsg);
  });

  process.on('unhandledRejection', err => {
    console.error('Uncaught Promise Error: ', err);
  });
};
