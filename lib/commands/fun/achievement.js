const commander = require('../../framework/commander.js');
const { Canvas } = require('canvas-constructor');
const snek = require('snekfetch');
const { resolve, join } = require('path');
const fsn = require('fs-nextra');

class achievement extends commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ['get', 'achieveget', 'achievementget'],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "ATTACH_FILES"],
      name: "achievement",
      category: "social",
      description: "Creates a Discord Themed Achievement.",
      extended: "*This generates a image of a achievement with the title you included! Really funny (100% Cringe)*",
      usage: "achievement <contents>"
    });  
  }

  async run(message, args, level, {MessageEmbed}) {
    let text = args.join(' ');
    if (text.length < 1) return message.reply('You must give an achievement description.');
    if (text.length > 22) return message.reply('I can only handle a maximum of 22 characters');
    if (message.mentions.users.first()) text = text.replace(/<@!?\d+>/, '').replace(/\n/g, ' ').trim();
    
    try {  
      let person = (message.mentions.users.first() || message.author).displayAvatarURL({format: 'png'});
      let plate = await fsn.readFile('./lib/assets/images/plate_achievement.png');
      let { body } = await snek.get(person);
      let picture = new Canvas(320, 64)
        .addImage(plate, 0, 0, 320, 64)
        .addImage(body, 16, 16, 32, 32, { type: 'round', radius: 16 })
        .restore()
        .setTextFont('12pt Minecraftia')
        .setColor('#FFFFFF')
        .addText(text, 60, 60)
        .toBuffer();
      await message.channel.send({ files: [{ attachment: picture, name: 'achievement.png' }] });
    } catch (err) {
      error(message, `• ${err.code}`, "• This is something thats never suppose to happen!", "• Retry the command again\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");;
      console.log(err);
    }
  }
}

module.exports = achievement;