const Commander = require('../../framework/commander.js');

class Avatar extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ["avatar"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "ATTACH_FILES"],
      name: "avatar",
      category: "social",
      description: "*Displays avatar profile picture*",
      extended: "*This displays the avatar picture of you! If you mentioned someone it will display their avatar.*",
      usage: "avatar <@mention>"
    });
  }

  async run(message, args, level, {MessageEmbed}) {
    try {
      const user = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.users.filter(u => u.id != this.commander.user.id).first() || message.author : message.mentions.users.first() || message.author;
      return message.channel.send({files: [{attachment: await this.url(user.id)}] })
    } catch (err) {
      error(message, `• ${err.code}`, "• This is something thats never suppose to happen!", "• Retry the command again\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");;
    } 
  }
};    

module.exports = Avatar;