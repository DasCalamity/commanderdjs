const Commander = require('../../framework/commander.js');
const { Canvas } = require('canvas-constructor');
const { resolve, join} = require('path');

class Blame extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ["blame"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "ATTACH_FILES", "MANAGE_MESSAGES"],
      name: "blame",
      category: "social",
      description: "*Blame a mentioned user for there doing!*",
      usage: "blame <@mention>"
    });
  }

  async run(message, args, level, {MessageEmbed}) {
    try {
      const person = message.mentions.members.first() || message.member;
      const size = new Canvas(130, 84)
        .setTextFont('700 32px bobTag')
        .measureText(person.displayName);
      const newSize = size.width < 130 ? 130 : size.width + 20;
      const blame = new Canvas(newSize, 84)
        .setTextFont('700 32px bobTag')
        .setColor('#B93F2C')
        .setTextBaseline('top')
        .setTextAlign('center')
        .addText('Blame', newSize/2, 5)
        .setColor('#F01111')
        .setTextBaseline('top')
        .setTextAlign('center')
        .addText(person.displayName, newSize/2, 45)
        .toBuffer();
      await message.channel.send({files: [{attachment: blame, name: 'blame.png'}]});
    } catch (err) {
      error(message, `• ${err.code}`, "• This is something thats never suppose to happen!", "• Retry the command again\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
      console.log(err);
    }
  }
}

module.exports = Blame;