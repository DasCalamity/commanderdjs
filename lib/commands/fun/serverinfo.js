const Commander = require('../../framework/commander.js');

class ServerInfo extends Commander {
  constructor(commander) {
    super(commander, {
	    enabled: true,
	    guildOnly: true,
	    aliases: ["server"],
	    permLevel: 0,
	    botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
	    name: "serverinfo",
	    category: "utility",
	    description: "*Gives info on the current Guild*",
	    usage: "info"
	  });
  }

  async run(message, args, level, {MessageEmbed}) {

    const online = message.guild.members.filter(m => m.presence.status === 'online');
  	const idle = message.guild.members.filter(m => m.presence.status === 'idle');
  	const dnd = message.guild.members.filter(m => m.presence.status === 'dnd');
  	const offline = message.guild.memberCount-online.size-idle.size-dnd.size; 
  	const bans = await message.guild.fetchBans().then(collection => collection.size);

  	const text = message.guild.channels.filter(m => m.type === 'text');
  	const voice = message.guild.channels.filter(m => m.type === 'voice');
  	const category = message.guild.channels.filter(m => m.type === 'category');

  	const afkTime = Math.floor((((message.guild.afkTimeout % 31536000) % 86400) % 3600) / 60)
  	
  	const verification = message.guild.verificationLevel.toString()
  		.replace("0", "None")
  		.replace("1", "Low")
  		.replace("2", "Medium")
  		.replace("3", "High")
  		.replace("4", "Super High");

	  const info = new MessageEmbed()
		  .setColor(0xC4010E)
		  .setThumbnail(message.guild.iconURL({format: 'png'}))
		  .addField('__**Server**__', `• Name - ${message.guild.name}\n• ID - *${message.guild.id}*\n• Owner - *${message.guild.owner.user.tag}*\n• Created - *${message.guild.createdAt.toDateString()}*\n• Region - *${message.guild.region.replace("-", "#")}*\n• Verification Level - *${verification}*`, true)
		  .addField('__**Members**__', `• Count - *${message.guild.memberCount}*\n• Online - *${online.size}*\n• Idle - *${idle.size}*\n• DnD - *${dnd.size}*\n• Offline - *${offline}*\n• Banned - *${bans}*`, true)
		  .addField('__**Channels/Roles**__', `• Channel Count - *${message.guild.channels.size}*\n• Text - *${text.size}*\n• Voice - *${voice.size}*\n• Categories - *${category.size}*\n• Role Count - ${message.guild.roles.size}`, true);

	  return message.channel.send({embed: info});
  }	
};

module.exports = ServerInfo;