const Commander = require('../../framework/commander.js');
const Canvas = require('canvas');
const snek = require('snekfetch');
const { readFile } = require('fs-nextra');
const GIFEncoder = require('gifencoder');
const streamToArray = require('stream-to-array');

class Triggered extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["triggered"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "BAN_MEMBERS", "MANAGE_MESSAGES", "EMBED_LINKS"],
      name: "triggered",
      category: "Moderation",
      description: "*You are Triggered!!!*",
      usage: "triggered <mention>"
    });
  }

  async run(message, args, level, {MessageEmbed}) { 
    const user = message.mentions.users.first() || message.author;
    const attachment = await this.getTriggered(user.displayAvatarURL({format: "png"}));    
    await message.channel.send({ files: [{ attachment, name: 'triggered.gif' }] });
  }
  
  async getTriggered(triggered) {
    const imgTitle = new Canvas.Image();
    const imgTriggered = new Canvas.Image();
    const encoder = new GIFEncoder(256, 256);
    const canvas = new Canvas.createCanvas(256, 256);
    const ctx = canvas.getContext('2d');
    const stream = encoder.createReadStream();
    const coord1 = [-25, -33, -42, -14];
    const coord2 = [-25, -13, -34, -10];

    imgTitle.src = await readFile('./lib/assets/images/plate_triggered.png');
    imgTriggered.src = await snek.get(triggered).then(res => res.body);
    
    encoder.start();
    encoder.setRepeat(0);
    encoder.setDelay(50);
    encoder.setQuality(200);
    
    for (let i = 0; i < 4; i++) {
      ctx.drawImage(imgTriggered, coord1[i], coord2[i], 300, 300);
      ctx.fillStyle = 'rgba(255 , 100, 0, 0.4)';
      ctx.drawImage(imgTitle, 0, 218, 256, 38);
      ctx.fillRect(0, 0, 256, 256);
      encoder.addFrame(ctx);
    }
  
    encoder.finish();
    return streamToArray(stream).then(Buffer.concat);
  }
}

module.exports = Triggered;