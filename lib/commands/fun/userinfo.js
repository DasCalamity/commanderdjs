const Commander = require('../../framework/commander.js');

class Profile extends Commander {
  constructor(commander) {
    super(commander, {
	  enabled: true,
	  guildOnly: true,
	  aliases: ["profile","userinfo", "user"],
	  permLevel: 0,
	  botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
	  name: "profile",
	  category: "social",
	  description: "*Shows user info*",
	  usage: "profile"
	});
  }

  async run(message, args, level, {MessageEmbed}) {

  	const user = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.users.filter(u => u.id != this.commander.user.id).first() || message.author : message.mentions.users.first() || message.author;
	const member = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.members.filter(u => u.id != this.commander.user.id).first() || message.member : message.mentions.members.first() || message.member;
    
	const profile = new MessageEmbed()
		.setColor(0xC4010E)
		.setThumbnail(await this.url(user.id))
		.addField('__**Account**__', `• ${user.tag}\n• ${user.id}\n• Created: *${user.createdAt.toDateString()}*\n• Status: ${member.presence.status || "offline"}\n• Game: ${user.presence.activity ? user.presence.activity.name : 'Nothing'}`, true)
		.addField('__**Server**__', `• Nickname - ${member.nickname || "No Nickname"}\n• Joined - ${member.joinedAt.toDateString()}\n• Highest Role - *${member.highestRole}*`, true)
	
	return message.channel.send({embed: profile});
  }
}				

module.exports = Profile;