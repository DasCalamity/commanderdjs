const Commander = require('../../framework/commander.js');

class Ban extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["ban", "Bun", "Bann"],
      permLevel: 2,
      botPerms: ["SEND_MESSAGES", "BAN_MEMBERS", "MANAGE_MESSAGES", "EMBED_LINKS"],
      name: "ban",
      category: "Moderation",
      description: "Bans a user from a guild.",
      extended: "\n• Bans a ``user`` from a guild/server\n• Must include a ``<user ID>`` or ``<@mention>``\n• Adding a ``reason`` is optional if You would like",
      usage: "ban <@mention> <reason>"
    });
  }

  async run(message, args, level, {MessageEmbed}) {
    
    const settings = this.commander.settings.get(message.guild.id);
    const modlog = message.guild.channels.find("name", settings.modLogChannel);
    const user = message.mentions.users.first();    
  
    if (!user) return error(message, `• Must include a user to ban!`, "• You did not provide a Valid user to ban!", "• Retry the command again but with mentioning a correct user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == message.author.id) return error(message, `• Why would you want to ban yourself silly goose...`, "• This occurred because you tried banning your server owner!", "• Retry the command again but with mentioning a bannable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == this.commander.user.id) return error(message, `• I cannot ban myself...`, "• This occurred because my role is equal to myself!", "• Retry the command again but with mentioning a bannable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == message.guild.owner.id) return error(message, `• I cannot ban the server leader silly goose...`, "• This occurred because you tried banning the server owner!", "• Retry the command again but with mentioning a bannable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (!message.guild.member(user).bannable) return error(message, `• I cannot ban this member...`, "• This occurred because you where trying to ban someone that has higher or equal to my rank/permission in the server!", "• Try banning someone who has a lower rank then me in the server!\n• Try moving my role up higher in the server so I can ban them\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");

    const member = message.guild.members.get(user.id);

    await member.ban();
    await message.delete();  
    
    if (!modlog) return error(message, `• There was no modlog channel found/set but ${member} was banned!`, `• This occurred because the guild leader has not set up a modLog Channel yet`, `• Please use \`\`\`\`${settings.prefix}set edit modLogChannel <Channel Name>\`\`\` too receive reports!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

    const reason = message.content.split(' ').slice(2).join(' ');
    const description = '**Username:** '+user.username+'#'+user.discriminator+'\n**UserID:** '+user.id+'\n**Action:** Was banned\n**banned By:** '+message.author.username+'\n**Reason: **'+(reason || ' N/A');
    const profileurl = await this.url(user.id);
    const casenumber = await this.caseNumber(modlog);
    
    await this.embed(message, description, modlog, casenumber, profileurl);
    return message.channel.send(""+member+" was banned :thumbsup:");
      
  }    
}

module.exports = Ban;