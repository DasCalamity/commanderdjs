const Commander = require('../../framework/commander.js');

class Case extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["action"],
      permLevel: 1,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      name: "case",
      category: "Moderation",
      description: "Pulls up recent case information",
      extended: "This pulls up a limit of 100 cases logged in your modLogChannel and displays it! This command is really helpful to see what was reason of a ban or who got banned by who!",
      usage: "case <case number>"
    });
  }	

  async run(message, args, level, {MessageEmbed}) {

    const settings = this.commander.settings.get(message.guild.id);
    const modlog = message.guild.channels.find("name", settings.modLogChannel);

    if (!modlog) return error(message, `• There was no modlog channel found/set!`, `• This occurred because the guild leader has not set up a modLog Channel yet`, `• Please use \`\`\`\`${settings.prefix}set edit modLogChannel <Channel Name>\`\`\` too receive reports!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

    const messages = await modlog.messages.fetch({limit: 100});
    const log = await messages.filter(m => m.author.id === this.commander.user.id && m.embeds[0] && m.embeds[0].type === 'rich' && m.embeds[0].footer && m.embeds[0].footer.text.startsWith('Case ' + args[0])).first();
    
    if (!log) return error(message, `• There was no cases found in modlogchannel found!`, `• This occurred because there might be no log placed in there or there was a internal error in my code!`, `• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

    await message.channel.send({embed: log.embeds[0]});
  }
}  
      
module.exports = Case;