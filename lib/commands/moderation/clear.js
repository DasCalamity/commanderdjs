const Commander = require('../../framework/commander.js');

class Clear extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["clear", "purge"],
      permLevel: 1,
      botPerms: ["SEND_MESSAGES", "MANAGE_MESSAGES", "EMBED_LINKS"],
      name: "clear",
      category: "Moderation",
      description: "Clears {x} of messages in a guild.",
      extended: "This command clears {X} amount of messages in a guild/server. You must include a **Ammount** or **@mention** Someone to clear messages! The bot cannot delete messages over 14 days since when they where created!",
      usage: "clear [ammount] & [@mention]"
    });
  }

  async run(message, args, level, {MessageEmbed}) {
    const settings = this.commander.settings.get(message.guild.id);
    let modlog = message.guild.channels.find("name", settings.modLogChannel);

    if (message.mentions.users.size) {

      const user = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.users.filter(u => u.id != this.commander.user.id).first() || 0 : message.mentions.users.first() || 0;
      const messages = await message.channel.messages.fetch({limit: 20}).then(m => m);
      const msg_array = messages.filter(m => m.author.id == user.id);
      
      await message.delete();
      await message.channel.bulkDelete(msg_array, true);
        
      if (!modlog) return error(message, `• There was no modlog channel found/set but 20 messages have been cleared!`, `• This occurred because the guild leader has not set up a modLog Channel yet`, `• Please use \`\`\`\`${settings.prefix}set edit modLogChannel <Channel Name>\`\`\` too receive reports!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

      const description = "**Username:** "+message.author.username+"#"+message.author.discriminator+"\n**UserID:** "+message.author.id+"\n**Action:** bulkDelete\n**Channel:** #"+message.channel.name+"\n**Cleared:** From "+user.tag+" (20 Messages)";
      const profileurl = await this.url(message.author.id);  
      const casenumber = await this.caseNumber(modlog);

      await message.channel.send("Deleted ``20`` messages from "+user+"");
      return await this.embed(message, description, modlog, casenumber, profileurl);
    
    } else {

      const params = args.join(' ');
    
      if (!params) return error(message, `• Must provide a number to delete X amount of messages!`, `• This occurred because you tried to delete messages but you did not provide a number`, `• Try providing a number or mention someone to delete messages\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);
      if (isNaN(params)) return error(message, `• You provided is not a number!`, `• This occurred because you tried to delete messages with a improper number!`, `• Try providing a proper number to delete messages!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);
      if (params > 99) return error(message, `• Cannot delete over 100 messages`, `• This occurred because you tried to delete over 100 messages which is restricted by Discord`, `• Try deleting less then 100 messages!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);
    
      const messagecount = parseInt(params) + 1;
      const messagecountdisplay = parseInt(params);
      const paramss = message.content.split(' ').slice(2).join(' ');

      await message.delete()  
      await message.channel.bulkDelete(messagecount, true)
   
      if (!modlog) if (!modlog) return error(message, `• There was no modlog channel found/set but ${messagecount} messages have been cleared!`, `• This occurred because the guild leader has not set up a modLog Channel yet`, `• Please use \`\`\`\`${settings.prefix}set edit modLogChannel <Channel Name>\`\`\` too receive reports!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

      const casenumber = await this.caseNumber(modlog);
      const description = "**Username:** "+message.author.username+"#"+message.author.discriminator+"\n**UserID:** "+message.author.id+"\n**Action:** bulkDelete\n**Channel:** #"+message.channel.name+"\n**Messagecount:** "+messagecount+"";
      const profileurl = await this.url(message.author.id);

      await message.reply("Deleted "+messagecountdisplay+" messages!!!");
      return await this.embed(message, description, modlog, casenumber, profileurl);
    
    }
  }  
};

module.exports = Clear;