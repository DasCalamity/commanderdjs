const Commander = require('../../framework/commander.js');

class Kick extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["kick"],
      botPerms: ["SEND_MESSAGES", "KICK_MEMBERS", "MANAGE_MESSAGES", "EMBED_LINKS"],
      permLevel: 1,
      name: "kick",
      category: "Moderation",
      description: "Kicks a user from the current Guild.",
      extended: "\n• Kicks a ``user`` from a guild/server\n• Must include a ``<user ID>`` or ``<@mention>``\n• Adding a ``reason`` is optional if You would like",
      usage: "kick [@mention] [reason]"
    });
  } 

  async run(message, args, level, {MessageEmbed}) {
    
    const settings = this.commander.settings.get(message.guild.id);
    const modlog = message.guild.channels.find("name", settings.modLogChannel);
    const user = message.mentions.users.first();    
  
    if (!user) return error(message, `• Must include a user to kick!`, "• You did not provide a Valid user to kick!", "• Retry the command again but with mentioning a correct user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == message.author.id) return error(message, `• Why would you want to kick yourself silly goose...`, "• This occurred because you tried kicking your server owner!", "• Retry the command again but with mentioning a kickable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == this.commander.user.id) return error(message, `• I cannot kick myself...`, "• This occurred because my role is equal to myself!", "• Retry the command again but with mentioning a kickable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (user.id == message.guild.owner.id) return error(message, `• I cannot kick the server leader silly goose...`, "• This occurred because you tried kickning the server owner!", "• Retry the command again but with mentioning a kickable user\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");
    if (!message.guild.member(user).kickable) return error(message, `• I cannot kick this member...`, "• This occurred because you where trying to kick someone that has higher or equal to my rank/permission in the server!", "• Try kicking someone who has a lower rank then me in the server!\n• Try moving my role up higher in the server so I can kick them\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support");

    const member = message.guild.members.get(user.id);
    
    await member.kick();
    await message.delete();  
    
    if (!modlog) return error(message, `• There was no modlog channel found/set but ${member} was kicked!`, `• This occurred because the guild leader has not set up a modLog Channel yet`, `• Please use \`\`\`\`${settings.prefix}set edit modLogChannel <Channel Name>\`\`\` too receive reports!\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support`);

    const reason = message.content.split(' ').slice(2).join(' ');
    const description = '**Username:** '+user.username+'#'+user.discriminator+'\n**UserID:** '+user.id+'\n**Action:** Was kicked\n**kicked By:** '+message.author.username+'\n**Reason: **'+(reason || ' N/A');
    const profileurl = await this.url(user.id);
    const casenumber = await this.caseNumber(modlog);
    
    await this.embed(message, description, modlog, casenumber, profileurl);
    return message.channel.send(""+member+" was kickned :thumbsup:");
        
  }    
}

module.exports = Kick;