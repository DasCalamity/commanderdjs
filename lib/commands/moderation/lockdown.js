const Commander = require('../../framework/commander.js');
const ms = require('ms');

class Lockdown extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["lock"],
      permLevel: 3,
      botPerms: ["SEND_MESSAGES", "MANAGE_CHANNELS"],
      name: "lockdown",
      category: "Moderation",
      description: "Locksdown message channel",
      extended: "This command Locksdown the current channel you activated the command in! Must include a time! You can also release/unlock the channel any time after activating the lockodwn!",
      usage: "lockdown <time>"
    });
  }

  async run(message, args, level, {MessageEmbed}) {

    if (!this.commander.lockit) this.commander.lockit = [];
    let time = args.join(' ') || await this.commander.awaitReply(message, "How long do you want to lockdown the channel?");
    if (time === false) return;
    let validUnlocks = ['release', 'unlock'];

    if (validUnlocks.includes(time)) {
      message.channel.overwritePermissions(message.guild.id, {SEND_MESSAGES: null}).then(() => {
        message.channel.send('Lockdown lifted.');
        clearTimeout(this.commander.lockit[message.channel.id]);
        delete this.commander.lockit[message.channel.id]}).catch(err => error(message, `• ${err.code || err}`, "• This is something thats never suppose to happen!", "• Retry the command again\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support"));
    } else {
      message.channel.overwritePermissions(message.guild.id, {SEND_MESSAGES: false}).then(() => {
        message.channel.send(`Channel locked down for ${ms(ms(time), { long:true })}`).then(() => {
          this.commander.lockit[message.channel.id] = setTimeout(() => {message.channel.overwritePermissions(message.guild.id, {SEND_MESSAGES: null}).then(message.channel.send('Lockdown lifted.')).catch(console.error);
          delete this.commander.lockit[message.channel.id]}, ms(time))
        }).catch(err => error(message, `• ${err.code || err}`, "• This is something thats never suppose to happen!", "• Retry the command again\n• Join The [Help Server](https://discord.gg/CFNUzK9) For Support"));
      })
    }
  }
};

module.exports = Lockdown;