const Commander = require('../../framework/commander.js');
const ms = require("ms");

class Mute extends Commander {
  constructor(commander) {
    super(commander, {
  	  enabled: true,
      guildOnly: true,
      aliases: ["mute"],
      permLevel: 1,
      botPerms: ["SEND_MESSAGES", "MANAGE_MESSAGES", "EMBED_LINKS"],
 	  name: "mute",
  	  category: "Moderation",
  	  description: "Mutes member in the guild",
  	  extended: "This command mutes a member in a guild/server. You must include a **@mention** and **Time**! You must have a muterole set by the command ``!set edit muterole {muterole name}`` This will add the muterole onto the mentioned user! You have to set up the muterole too your preferences and adjust the role so they cannot chat and join voicechannels!",
 	  usage: "mute"
	});
  }

  async run(message, args, level, {MessageEmbed}) {

  	const user = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.users.filter(u => u.id != this.commander.user.id).first() || message.author : message.mentions.users.first() || message.author;
	const member = message.content.split(' ')[0].includes(this.commander.user.id) ? message.mentions.members.filter(u => u.id != this.commander.user.id).first() || message.member : message.mentions.members.first() || message.member;
    
	if (!user) return message.reply("``ERROR`` : Must include a **<userid>** Or **[@mention]**!");
	if (!member) return message.reply("``ERROR`` : Must include a **<userid>** Or **[@mention]**!");
	
	const settings = await this.commander.settings.get(message.guild.id);
	const modlog = message.guild.channels.find("name", settings.modLogChannel);
	const muterole = await this.commander.guilds.get(message.guild.id).roles.find('name', settings.muterole);
    const time = message.content.split(' ').slice(2).join(' ');

	if (!muterole) return message.reply("``ERROR`` : There was no **muterole** found/set! Please use ``"+settings.prefix+"set edit {muterole name}``! Also make sure you make sure the muterole has no perms!");
	if (!time) return message.reply("``Error`` : Must include a **<Time>** for the member too be muted!");

	const description = `**Username:** ${user.username}#${user.discriminator}\n**UserID:** ${user.id}\n**Action:** Was Muted\n**Muted By:** ${message.author.username}\n**Time:** ${ms(ms(time), {long: true})}`
	const userURL = await this.url(user.id)

    await message.delete();
	await member.addRole(muterole);

	if (!modlog) { 
		await message.channel.send("``ERROR`` : There was no modlog channel found/set! Please use ```"+settings.prefix+"set edit modLogChannel {Channel Name}``` too receive reports!");
	} else {
		const casenumber = await this.caseNumber(modlog);
		await message.channel.send(`${member} was Muted for ${ms(ms(time), {long: true})}`);
    	await this.embed(message, description, modlog, casenumber, userURL);
	}

	await setTimeout(() => {
		member.removeRole(muterole);
		return message.channel.send(`${member} was unmuted!!!`);
	}, ms(time));
  }	
};

module.exports = Mute;