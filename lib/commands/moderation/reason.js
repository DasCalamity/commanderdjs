const Commander = require('../../framework/commander.js');

class Reason extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["reason"],
      permLevel: 1,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      name: "reason",
      category: "Moderation",
      description: "Edits a recent log reason to what you want!",
      extended: "*Edits a recent mod log reason! This is a cool command if you made mistakes when making the reason!*", 
      usage: "case <case number> <new reason>"
    });
  }

  async run(message, args, level, {MessageEmbed}) { 
    const caseNumber = args.shift();
    const newReason = args.join(' ');
    const settings = message.settings;
    const modlog = message.guild.channels.find('name', settings.modLogChannel);
    
    if (!modlog) return message.reply(`Cannot find the \`${settings.modLogChannel}\` channel.`);
    
    const messages = await modlog.messages.fetch({limit: 100});
    const caseLog = await messages.find(m => m.embeds[0] && m.embeds[0].footer && m.embeds[0].footer.text === `Case ${caseNumber}`);
    const logMsg = await modlog.messages.fetch(caseLog.id);
    const logEmbed = logMsg.embeds[0];
    const parts = logEmbed.description.split('\n**Reason: **');
    const updatedReason = parts[0] + `\n**Reason: **` + newReason;

    const embed = new MessageEmbed()
      .setAuthor(logEmbed.author.name, message.guild.iconURL({format: 'png'}))
      .setColor(0xC4010E)
      .setFooter(logEmbed.footer.text, this.commander.user.avatarURL({format: 'png'}))
      .setThumbnail(logEmbed.thumbnail.url.toString())
      .setTimestamp()
      .setDescription(updatedReason);  
    
    await logMsg.edit({embed});

    return await message.channel.send("Reason updated!");
  }
}    

module.exports = Reason;