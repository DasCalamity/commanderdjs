const Commander = require('../../framework/commander.js');

class Resume extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
  	  guildOnly: true,
      aliases: ["resume", "r"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES"],
  	  name: "resume",
      category: "Youtube",
      description: "Resumes music in voicechat",
      usage: "resume"
    });
  }

  async run(message, args, level) {
 
  	this.commander.voiceConnections.get(message.guild.id).dispatcher.resume();
  
  }
};

module.exports = Resume;