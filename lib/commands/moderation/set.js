const Commander = require('../../framework/commander.js');

class Settings extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["set", "settings", "setting"],
      permLevel: 3,
      botPerms: ["SEND_MESSAGES"],
      name: "set",
      category: "utility",
      description: "View or change settings for your server.",
      usage: "set <view/get/edit> <key> <value>" 
    });
  } 

  async run(message, [action, key, ...value], level, {MessageEmbed}) {

    const settings = this.commander.settings.get(message.guild.id);

    if (action === "edit") {
      if (!key) return message.reply("Please specify a key to edit");
      if (!settings[key]) return message.reply("This key does not exist in the settings");
      if (!value) return message.reply("Please specify a new value");
      settings[key] = value.join(" ");
      this.commander.settings.set(message.guild.id, settings);
      return message.reply(`${key} successfully edited to ${value.join(" ")}`);
    } 
    if (action === "get") {
      if (!key) return message.reply("Please specify a key to view");
      if (!settings[key]) return message.reply("This key does not exist in the settings");
      return message.reply(`The value of ${key} is currently ${settings[key]}`);
    } 
    const { inspect } = require("util");
    return message.channel.send(inspect(settings), {code: "json"});
  }
};

module.exports = Settings;
