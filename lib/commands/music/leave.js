const Commander = require('../../framework/commander.js');

class Leave extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["leave"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES"],
      name: "leave",
      category: "Youtube",
      description: "Leaves Guild Voice Channel.",
      extended: "Leaves Guild Voice Channel. (Bot must be in one)",
      usage: "leave"
    });
  }

  async run(message, args, level, {MessageEmbed}) {

    let voiceChannel = message.member.voiceChannel;
    if (voiceChannel) voiceChannel.leave();
  
  }
};  

module.exports = Leave;