const Commander = require('../../framework/commander.js');

class Pause extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["pause", "p"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES"],
      name: "pause",
      category: "Youtube",
      description: "Pauses music in Guild voicechat",
      usage: "pause"
    });
  }

  async run(message, args, level, {MessageEmbed}) {

    this.commander.voiceConnections.get(message.guild.id).dispatcher.pause()

  }
}; 

module.exports = Pause;