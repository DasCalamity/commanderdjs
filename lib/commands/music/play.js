const ytdl = require('ytdl-core');
const search = require('youtube-search')
const Commander = require('../../framework/commander.js');

const dispatcher = null;
let isPlaying = false;

let bot;

function play(connection, message) {

  let server = servers[message.guild.id]

    server.dispatcher = connection.playStream(ytdl(server.queue[0], {filter: 'audioonly'}))
    server.queue.shift();
    server.dispatcher.on("end", function() {

    if (!server.queue[0]) return isPlaying = false;
    
    return play(connection, message);
  });
};

let servers = {};

class Play extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["play"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "SPEAK", "CONNECT", "EMBED_LINKS"],
      name: "play",
      category: "Youtube",
      description: "Plays music in Guild voicechat.",
      usage: "play [song]"
    });
    bot = this.commander;
  } 

    async run(message, args, level, {MessageEmbed}) {

      if (!args) return message.channel.send("``Error`` : Must include a link or Video");

      if (!servers[message.guild.id]) servers[message.guild.id] = {
        queue: []
      }

      let server = servers[message.guild.id]

      const opts = {
        maxResults: this.commander.config.youtube.maxResults,
        key: this.commander.config.youtube.key
      };

      search(args, opts, function(errr, results) {

        if(errr) return message.channel.send(errr);

        let bam = results[0].link

        if(!bam) return message.channel.send("``Error`` : Link was not valid!");

        let queue = server.queue;

        if (server.queue.length > 0 || isPlaying) {

          server.queue.push(""+bam+"");

          const youtubevideos = new MessageEmbed()
            .setTitle(""+results[0].title+"")
            .setAuthor(""+message.guild.name+" | "+message.guild.id+"", message.guild.iconURL({format: 'png'}))
            .setColor(0xC4010E)
            .setImage(""+results[0].thumbnails.medium.url+"")
            .setURL(""+results[0].link+"")
            .setFooter('commander.log', bot.user.avatarURL({format: 'png'}))
            .setTimestamp()
            .setDescription('**Video Added To Queue:** '+results[0].title+'\n**Channel:** '+results[0].channelTitle+'\n**Requester:** '+message.author.username+'#'+message.author.discriminator+'');

          return message.channel.send({embed: youtubevideos});

        }

        server.queue.push(""+bam+"");

	      message.member.voiceChannel.join().then(function(connection) {

            play(connection, message);

            const youtubevideo = new MessageEmbed()
              .setTitle(""+results[0].title+"")
              .setAuthor(""+message.guild.name+" | "+message.guild.id+"", message.guild.iconURL({format: 'png'}))
              .setColor(0xC4010E)
              .setImage(""+results[0].thumbnails.medium.url+"")
              .setFooter('commander.log', bot.user.avatarURL({format: 'png'}))
              .setURL(""+results[0].link+"")
              .setDescription('**Now Playing:** '+results[0].title+'\n**Channel:** '+results[0].channelTitle+'\n**Requester:** '+message.author.username+'#'+message.author.discriminator+'');
    
            isPlaying = true;

            return message.channel.send({embed: youtubevideo});
        });
    });
  }
}

module.exports = Play;