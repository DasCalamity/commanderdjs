const Commander = require('../../framework/commander.js');

class Skip extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["skip", "s"],
      botPerms: ["SEND_MESSAGES"],
      permLevel: 0,
      name: "skip",
      category: "Youtube",
      description: "skips music in a voicechat.",
      usage: "skip"
    });
  }


  async run(message, args, level) {

    this.commander.voiceConnections.get(message.guild.id).dispatcher.end();
  
  }
};

module.exports = Skip