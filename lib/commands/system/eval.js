const Commander = require('../../framework/commander.js');

class Eval extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ["eval", "exe", "e"],
      permLevel: 5,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      name: "eval",
      category: "Devs",
      description: "Evaluates arbitrary javascript.",
      extended: "This is only for the Devs because this exeicutes code and files!",
      usage: "eval [code]"
    });
  }

  async run(message, args, level, {MessageEmbed}) {

    const code = args.join(" ") || await this.commander.awaitReply(message, "What would you like to Evaluate?");
    if (code === false) return;

    try {
      const commander = this.commander;
      const evaled = eval(code);
      const clean = await this.clean(evaled);
      
      if (clean.length > 1000) {
        const hastebin = await this.commander.hastebin(clean, "js").then(link => link);
        const evals = new MessageEmbed().setColor(0xC4010E).setDescription(`:inbox_tray: **__Input:__**\`\`\`js\n${code}\`\`\`\n:outbox_tray: **__Output:__** *${typeof evaled}* \`\`\`js\n${hastebin}\`\`\``);
        return message.channel.send({embed: evals});
      }

      const evalclean = new MessageEmbed().setColor(0xC4010E).setDescription(`:inbox_tray: **__Input:__**\`\`\`js\n${code}\`\`\`\n:outbox_tray: **__Output:__** *${typeof evaled}* \`\`\`js\n${clean}\`\`\``);
      return message.channel.send({embed: evalclean});
    
    } catch (err) {
      const evalclean = new MessageEmbed().setColor(0xC4010E).setDescription(`:inbox_tray: __**Input:**__ \`\`\`js\n${code}\`\`\`\n:outbox_tray: **__Output:__** \`\`\`xl\n${err}\`\`\``);
      await message.channel.send({embed: evalclean});
    }
  }  
};

module.exports = Eval;