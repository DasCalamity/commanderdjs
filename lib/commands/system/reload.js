const Commander = require('../../framework/commander.js');

class Reload extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ["reload"],
      permLevel: 5,
      botPerms: ["SEND_MESSAGES"],
      name: "reload",
      category: "Devs",
      description: "Reloads a command from library.",
      usage: "reload [command]"
    });
  }

  async run(message, args, level) {

    if (!args || args.size < 1) return message.channel.send('Must provide a command to reload.');

    let command;
    if (this.commander.commands.has(args[0])) {
      command = this.commander.commands.get(args[0]);
    } else if (this.commander.aliases.has(args[0])) {
      command = this.commander.commands.get(this.commander.aliases.get(args[0]));
    }
    
    if (!command) return message.channel.send(`The command \`${args[0]}\` doesn't seem to exist, nor is it an alias. Try again!`);
    if (command.db) await command.db.close();

    command = command.help.name;

    delete require.cache[require.resolve(`./${command}.js`)];
    const cmd = new (require(`./${command}`))(this.commander);
    this.commander.commands.delete(command);
    if (cmd.init) cmd.init(this.commander);
    this.commander.aliases.forEach((cmd, alias) => {
      if (cmd === command) this.commander.aliases.delete(alias);
    });
    this.commander.commands.set(command, cmd);
    cmd.conf.aliases.forEach(alias => {
      this.commander.aliases.set(alias, cmd.help.name);
    });

    message.channel.send(`The command \`${command}\` has been reloaded`);
  }
};    

module.exports = Reload;