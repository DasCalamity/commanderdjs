const Commander = require('../../framework/commander.js');
const nodemon = require('nodemon');

class Restart extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ['restart'],
      permLevel: 10,
      botPerms: ["SEND_MESSAGES"],
      name: "restart",
      category: "Secret",
      description: "Restarts bot",
      usage: "restart"
    });
  }	

  async run(message, args, level, {MessageEmbed}) {
    try {
      await message.reply('Bot is shutting down.');
      this.commander.commands.forEach(async cmd => await this.commander.unloadCommand(cmd));
      await nodemon({script: 'commander.js'}).emit('restart');
    } catch (e) {
      console.log(e);
    }
  }
};

module.exports = Restart;