const Commander = require('../../framework/commander.js');

class Send extends Commander {
  constructor(commander) {
    super(commander, {
  	  enabled: true, 
      guildOnly: true,
      aliases: ["send"],
      permLevel: 10,
      botPerms: ["SEND_MESSAGES"],
  	  name: "send",
      category: "Devs",
      description: "send message.contents",
      usage: "say [message contents]"
    });
  }


  async run(message, args, level) {

	  this.commander.guilds.forEach(g => {
      try {
        g.channels.find(c => c.type === 'text').send(message.content.split(' ').slice(1).join(' '))
      } catch (err) {
        console.log("Some Servers are restricted")
      }
	  })
  }
};  

module.exports = Send;