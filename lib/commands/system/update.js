const Commander = require('../../framework/commander.js');
const { promisify } = require('util');
const exec = promisify(require('child_process').exec);
const path = require('path');

class Update extends Commander {
  constructor(commander) {
    super(commander, {
  	  enabled: true,
      guildOnly: false,
  	  aliases: ["update"],
  	  permLevel: 10,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
  	  name: "update",
  	  category: "utility",
      description: "update",
      extended: "update",
      usage: "update"
    })
  }

  async run(message, args, level, {MessageEmbed}) {
    const { stdout, stderr, err } = await exec(`git pull ${require('../../../package.json').repository.url.split('+')[1]}`, { cwd: path.join(__dirname, '../../') }).catch(err => ({ err }));
    
    if (err) return console.error(err);
    
    let embed = {
      fields: []
    };
    
    if (stdout) embed.fields.push({
      name: "STDOUT",
      value: stdout
    });
    
    if (stderr) embed.fields.push({
      name: "STDEER",
      value: stderr
    });

    await message.channel.send({embed: embed})
  
    if (!stdout.toString().includes('Already up-to-date.')) {
      this.commander.commands.get('restart').run(message, args, level, {MessageEmbed});
    }
  }
}

module.exports = Update;