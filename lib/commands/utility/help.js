const Commander = require('../../framework/commander.js');

class Help extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ['help', 'h', 'halp'],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      name: "help",
      category: "utility",
      description: "Gives info on a current Command.",
      extended: "Gives handy info on a command or just gives you a list of all commands!\n**Enabled:** means if its enabled or not!\n**Aliases:** States what names you can type to execute the command!\n**Level:** Displays what perm level you need to use thid command1",
      usage: "help <command name>"
    });
  };

  async run(message, args, level, {MessageEmbed}) {
    const settings = message.guild ? this.commander.settings.get(message.guild.id) : this.commander.config.defaultSettings;
    message.settings = settings;

    if (args[0]) {
      let c = this.commander.commands.get(args[0]) || this.commander.commands.get(this.commander.aliases.get(args[0].toLowerCase()));
      if (!c) return message.channel.send("``ERROR`` : not a valid command name!");
      const helps = new MessageEmbed().setColor(0xC4010E).setThumbnail('https://i.imgur.com/WPuLt3u.gif').setDescription("**Command Name:** *"+c.help.name+"*\n**Catagory:** *"+c.help.category+"*\n**Aliases:** "+c.conf.aliases.join(", ")+"\n**Usage:** *"+settings.prefix+""+c.help.usage+"*\n**Enabled:** *"+c.conf.enabled+"*\n**Guildonly:** *"+c.conf.guildOnly+"*\n**Permlevel:** *"+c.conf.permLevel+"*\n**Description:** "+(c.help.extended || c.help.description)+"");
      return message.channel.send({embed: helps});
    } else {

      let social = this.commander.commands.filter(c => c.help.category === 'social');
      let socialinfo = social.map(si => `• ${settings.prefix}${si.help.name}`).join('\n');
      let Moderation = this.commander.commands.filter(m => m.help.category === 'Moderation');
      let Moderationinfo = Moderation.map(mi => `• ${settings.prefix}${mi.help.name}`).join('\n');
      let Youtube = this.commander.commands.filter(m => m.help.category === 'Youtube');
      let Youtubeinfo = Youtube.map(yi => `• ${settings.prefix}${yi.help.name}`).join('\n');
      let utility = this.commander.commands.filter(u => u.help.category === 'utility');
      let utilityinfo = utility.map(ui => `• ${settings.prefix}${ui.help.name}`).join('\n');
      let Coding = this.commander.commands.filter(c=> c.help.category === 'Coding Help');
      let CodingInfo = Coding.map(ci => `• ${settings.prefix}${ci.help.name}`).join('\n');
      
      const help = new MessageEmbed()
        .setColor(0xC4010E)
        .setDescription("**Commander** is a multipurpose bot and runs off of Javascript! If you wish too learn more about a command please use ``"+settings.prefix+"help {command name}`` too learn more about a command! If you have any issues or bugs too report join the Commander Help server https://discord.gg/2aSrp6F !")
        .addField('__**Moderation**__', Moderationinfo, true)
        .addField('__**Utility**__', utilityinfo, true)
        .addField('__**Youtube/Music**__ [beta]', Youtubeinfo, true)
        .addField('__**Social**__', socialinfo, true)
        .addField('__**Coding Commands**__', CodingInfo, true);
     

      if (message.guild) {
        await message.reply("OK! Sent a list of Commands!");
        return this.commander.users.get(message.author.id).send({embed: help});
      }

      if (!message.guild) {
        return message.channel.send({embed: help});
      }
    } 
  }  
};

module.exports = Help;