const Commander = require('../../framework/commander.js');
const { version } = require('discord.js');
const moment = require("moment");
require("moment-duration-format");

class Stats extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: false,
      aliases: ["stats", "info"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      name: "stats",
      category: "utility",
      description: "*Gives some useful bot statistics*",
      usage: "stats"
    });
  } 

  async run(message, args, level, {MessageEmbed}) {
    const settings = message.guild ? this.commander.settings.get(message.guild.id) : this.commander.config.defaultSettings;
    message.settings = settings;
    const duration = await moment.duration(this.commander.uptime).format(" D [days], H [hrs], m [mins]");
    const evaled = await eval(this.commander.config.devs);
    const done = await this.clean(evaled.join(', '));
    const devs = done.slice(1, -1);
    const stats = new MessageEmbed()
      .setColor(0xC4010E)
      .setThumbnail('https://i.imgur.com/WPuLt3u.gif')
      .addField('__**System**__', `• Mem Usage - *${(process.memoryUsage().heapUsed / 1024 / 1024).toFixed(2)} MB*\n• Uptime - *${duration}*\n• OS - *${process.env.OS}(${process.arch})*\n• Discord.js - *v${version}*\n• Node.js - *${process.version}*`, true)
      .addField('__**Discord**__', `• Servers - *${this.commander.guilds.size.toLocaleString()}*\n• Channels - *${this.commander.guilds.reduce((p, c) => p + c.channels.size, 0)}*\n• Roles - *${this.commander.guilds.reduce((p, c) => p + c.roles.size, 0)}*\n• Emojis - *${this.commander.emojis.size.toLocaleString()}*\n• Users - *${this.commander.users.size}/${this.commander.guilds.reduce((p, c) => p + c.memberCount, 0)}*`, true)
    return await message.channel.send({embed: stats});
  }
};

module.exports = Stats;