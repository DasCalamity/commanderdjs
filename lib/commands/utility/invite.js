const Commander = require('../../framework/commander.js');

class Invite extends Commander {
  constructor(commander) {
    super(commander, {
  	  enabled: true,
      guildOnly: false,
  	  aliases: ["invite", "link", "inv"],
  	  permLevel: 0,
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
  	  name: "invite",
  	  category: "utility",
      description: "Generates a Bot invite link.",
      extended: "Generates a invite link to invite Commander into your own server!",
      usage: "invite"
    });
  }

  async run(message, args, level, {MessageEmbed}) {
	  const link = await this.commander.generateInvite(["CREATE_INSTANT_INVITE", "KICK_MEMBERS", "BAN_MEMBERS", "MANAGE_CHANNELS", "MANAGE_GUILD", "ADD_REACTIONS", "VIEW_AUDIT_LOG", "SEND_MESSAGES", "SEND_TTS_MESSAGES", "MANAGE_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "READ_MESSAGE_HISTORY", "MENTION_EVERYONE"]).then(link => link);
      const links = new MessageEmbed()
			 .setColor(0xC4010E)
			 .setThumbnail('https://i.imgur.com/WPuLt3u.gif')
			 .setDescription(`**Invite List**\n� [Commander Invite Link](${link})\n� [Commander Help Sever Link](https://discord.gg/CFNUzK9)\n\nIf you have any other concerns message *__Calamity#5938__*!`);
	  return message.channel.send({embed: links});	
  }
};

module.exports = Invite;