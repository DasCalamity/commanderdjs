const Commander = require('../../framework/commander.js');

class Level extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true,
      guildOnly: true,
      aliases: ["level", "lvl", "mylevel"],
      permLevel: 0,
      botPerms: ["SEND_MESSAGES"],
      name: "level",
      category: "utility",
      description: "Displays your permission level.",
      extended: "Displays your permission level in Guild! Current level are\n``Level 0:`` No perms!\n``Level 1:`` Moderator/Kick perms\n``Level 2:`` Admin/ban Perms\n``Level 3:`` Server Owner perms\n``Level 5/10:`` Dev perms",
      usage: "mylevel"
    });
  }

  async run(message, args, level, {MessageEmbed}) {

   return message.channel.send(`Your permission level is: ${level}`);
  
  }
};

module.exports = Level;