const Commander = require('../../framework/commander.js');
const npmSearch = require('npm-module-search');
const npmPackageStars = require('npm-package-stars');

class Npm extends Commander {
  constructor(commander) {
    super(commander, {
  	  enabled: true,
  	  guildOnly: false,
  	  aliases: ["npm"],
      botPerms: ["SEND_MESSAGES", "EMBED_LINKS"],
      permLevel: 0,
  	  name: "npm",
  	  category: "Coding Help",
      description: "Gives info on a certain NPM Module",
  	  usage: "npm [module name]"
	  });
  }

  async run(message, args, level, {MessageEmbed}) {

	  let reason = args.join(' ') || await this.commander.awaitReply(message, "What NPM Package do you want to search for?");
    
    if (reason === false) return;
	  if (reason.length >= 30) return message.channel.send("``Error`` : No Npm Module Found!!!");

	  npmSearch.search(reason, function (err, modules) {
		  if (!module || err) return message.channel.send("``Error`` : No Npm Module Found!!!");
		  if (!modules[0].name) return message.channel.send("``Error`` : No Npm Module Found!!!");
		  
      npmPackageStars(modules[0].name.toString()).then(stars => {
			  const module = new MessageEmbed()
  				.setColor(0xC4010E)
          .setThumbnail('https://i.imgur.com/WPuLt3u.gif')
  				.setDescription(`**Module Name:** ${modules[0].name} \n**Version:** ${modules[0].version}\n**Author:** ${modules[0].author}\n**Stars:** ${stars}\n**Link:** https://www.npmjs.com/package/${modules[0].name}\n**Description:** ${modules[0].description}`);
   		  return message.channel.send({embed: module});
		  })
    })
  }
};	

module.exports = Npm;