const Commander = require('../../framework/commander.js');

class Say extends Commander {
  constructor(commander) {
    super(commander, {
      enabled: true, 
      guildOnly: true,
      aliases: ["say", "s"],
      permLevel: 3,
      botPerms: ["SEND_MESSAGES", "MANAGE_MESSAGES"],
      name: "say",
      category: "Devs",
      description: "Repeats message.contents",
      usage: "say [message contents]"
    });
  }

  async run(message, args, level) {
    await message.channel.send(message.content.split(' ').slice(1).join(' '));
  	await message.delete();
  }
};

module.exports = Say;