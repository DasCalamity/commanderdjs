const { MessageEmbed } = require('discord.js');

module.exports = class {
  constructor(commander) {
    this.commander = commander;
  }

  async execute(guild) {

  	await this.commander.settings.set(guild.id, this.commander.config.defaultSettings);
  	const WelcomeMessage = new MessageEmbed()
      .setAuthor('Commander The Top Line Bot', this.commander.user.avatarURL({format: 'png'}))
      .setColor(0xFF0000)
      .setThumbnail('https://i.imgur.com/WPuLt3u.gif')
      .addField("Thank You for Inviting Commander!", "Commander is a multipurpose bot that runs off of [Discord.JS](https://discord.js.org/#/) and [Node.JS](https://nodejs.org/en/) Our main objective is too make life easier and funnier for users on Discord!")
      .addField("Commander Prefixes", 'Yes! There is a **s** at the end of prefix,\nthere are two prefixes and I will explain both of them here. Right now the settings prefix is defaulted ``!``, You can change the setting prefix to what you want to use\n```diff\n- Prefix #1 is mentioning\n+ Example: @Commander#0800 help\n- Prefix #2 is set by the guild leader!\n+ Change it: !set edit prefix <new prefix>\n+ Example: !help```');
  	await guild.channels.find(c => c.type === 'text').send({embed: WelcomeMessage});

  }
}