module.exports = class {
  constructor(commander) {
    this.commander = commander;
  }

  async execute(guild) {
  	this.commander.settings.delete(guild.id);
  }
};