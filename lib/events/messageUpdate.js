module.exports = class {
  constructor(commander) {
    this.commander = commander;
  }

  async execute(oldmessage, message) {

    if (message.author.bot) return;

    const settings = message.guild ? this.commander.settings.get(message.guild.id) : this.commander.config.defaultSettings;

    message.settings = settings;

    const prefixes = [settings.prefix, `${this.commander.user} `];
    let prefix = false;
  
    if (message.content.startsWith(this.commander.user.toString())) {
        prefix = this.commander.user.toString();
    } else {
        for (const thisPrefix of prefixes) {
          if (message.content.indexOf(thisPrefix) == 0) prefix = thisPrefix;
        }
    }

    if (!prefix) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/g); 
    const command = args.shift().toLowerCase();
    const {MessageEmbed} = require('discord.js');
    const level = this.commander.permlevel(message);
    const cmd = this.commander.commands.get(command) || this.commander.commands.get(this.commander.aliases.get(command));

    if (cmd && !message.guild) {
      if (level < cmd.conf.permLevel) return message.reply("``ERROR`` : You do not have perms XD");
      if (cmd.conf.guildOnly === true) return message.reply("This command is unavailable via private message. Please run this command in a guild.");
      if (cmd.conf.enabled === false) return message.reply("Sorry the command has been disabled by Calamity");
      this.commander.log("Commander", `${message.author.username}#${message.author.discriminator} (${message.author.id}) (DM) ran command ${cmd.help.name}`, "Log"); 
      cmd.run(message, args, level, {MessageEmbed}).catch(error => {
        message.channel.send('``Error`` : Something Happened! This is mostly on our side of the issue!');
        console.log(error)
      })  
    }

    if (cmd && message.guild) {
      const mPerms = this.commander.permCheck(message, cmd.conf.botPerms);
      if (mPerms.length) return message.channel.send("``Error`` Do not have the following permissions **"+mPerms.join(', ')+"**");
      if (level < cmd.conf.permLevel) return message.reply("``ERROR`` : You do not have perms XD");
      if (cmd.conf.enabled === false) return message.reply("Sorry the command has been disabled by Calamity");
      this.commander.log("Commander", `${message.author.username}#${message.author.discriminator} (${message.author.id}) (${message.guild.name}) ran command ${cmd.help.name}`, "Log") 
      cmd.run(message, args, level, {MessageEmbed}).catch(error => {
        message.channel.send('``Error`` : Something Happened! This is mostly on our side of the issue!');
        console.log(error)
      })  
    } 
  }    
}  
