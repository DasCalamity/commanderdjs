module.exports = class {
  constructor(commander) {
    this.commander = commander;
  }

  async setGame(index, that) {
    let games = ["Servers: "+that.commander.guilds.size+"", "Users: "+that.commander.users.size+"", "Coded By: Calamity#5938", "Default Prefix: "+that.commander.config.defaultSettings.prefix+""]
    index %= 4;
    await that.commander.user.setActivity(games[index]);
    await index++;
    await setTimeout(that.setGame, 15000, index, that);
    return;
  };

  async execute(index) {
    await wait(1000);
    await this.commander.log("Commander", `Ready to serve ${this.commander.users.size} users in ${this.commander.guilds.size} servers.`, "Ready");
    await this.commander.guilds.filter(g => !this.commander.settings.has(g.id)).forEach(g => this.commander.settings.set(g.id, this.commander.config.defaultSettings));
    await this.setGame(0, this);
  };
}
