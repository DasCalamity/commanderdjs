module.exports = class Commander {
  constructor(commander, {
    name = null,
    description = null,
    category = 'general',
    usage = 'No usage provided.',
    botPerms = [],
    guildOnly = false,
    enabled = true,
    extended = null,
    aliases = [],
    permLevel = 0
  }) {
    this.commander = commander;
    this.conf = {
      enabled,
      guildOnly,
      aliases,
      permLevel,
      botPerms
    };
    this.help = {
      name,
      category,
      description,
      extended,
      usage
    };
  }

  async url(user) {
    const userprofile = await this.commander.users.fetch(user).then(user => user.displayAvatarURL().replace("webp", "png"));
    return userprofile;
  }  

  async clean(text) {
    if (text && text.constructor.name == "Promise")
      text = await text;
    if (typeof evaled !== "string")
      text = require("util").inspect(text, {depth: 0});

    text = text
      .replace(/`/g, "`" + String.fromCharCode(8203))
      .replace(/@/g, "@" + String.fromCharCode(8203))
      .replace(this.commander.token, "XDVYOUkO_2G4Qv3ARElWetW_tjNDFUNNY-QFTm6YGtzq9PH4UtG0")
      .replace(this.commander.config.youtube.key, "HA-KO-NYOUAGSJS_GFHGFUNNYGHJGH")
      .replace(this.commander.config.devs[0], "Calamity#5938")
      .replace(this.commander.config.devs[1], "VoidyRäh#8969")
      .replace(undefined, "Complete(undefined return)");

    return text;
  };

  async embed(message, description, modlog, casenumber, user) {
    const {MessageEmbed} = require('discord.js');
    const log = new MessageEmbed()
      .setAuthor(message.guild.name, message.guild.iconURL({format: 'png'}))
      .setColor(0xC4010E)
      .setFooter('Case ' + casenumber, this.commander.user.avatarURL({format: 'png'}))
      .setThumbnail(user)
      .setTimestamp()
      .setDescription(description);
    return this.commander.channels.get(modlog.id).send({embed: log}); 
  } 

  async caseNumber(modLog) {
    const messages = await modLog.messages.fetch({limit: 5});
    const log = messages.filter(m => m.author.id === this.commander.user.id
      && m.embeds[0]
      && m.embeds[0].type === 'rich'
      && m.embeds[0].footer
      && m.embeds[0].footer.text.startsWith('Case')
    ).first();
    if (!log) return 1;
    const thisCase = /Case\s(\d+)/.exec(log.embeds[0].footer.text);
    return thisCase ? parseInt(thisCase[1]) + 1 : 1;
  }
}  